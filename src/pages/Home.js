import React from 'react'
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'
import { Container } from 'react-bootstrap'
 
 
const Home = () => {
   const pageData = {
       title: "Traveller's Nook",
       content: "Polearms for everyone, everywhere",
       destination: "/products",
       label: "Browse Products"
   };
 
   return(
       <React.Fragment>
           <Banner data={pageData}/>
           <Container fluid>
               <h2 className="text-center mb-4">Featured Polearms</h2>
               {/* <Highlights/> */}
           </Container>
       </React.Fragment>
   );
}
export default Home;
